//
//  AppDelegate.h
//  NSUserDefaultsExample
//
//  Created by Kvana Mac Pro 2 on 12/15/15.
//  Copyright © 2015 Kvana Mac Pro 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

