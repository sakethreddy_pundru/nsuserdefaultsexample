//
//  SignUpViewController.m
//  NSUserDefaultsExample
//
//  Created by Kvana Mac Pro 2 on 12/15/15.
//  Copyright © 2015 Kvana Mac Pro 2. All rights reserved.
//

#import "SignUpViewController.h"
#import "Constans.h"
#import "ViewController.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController{
    UITextField *userNameTF;
    UITextField *passwordTextFiled;
    
    UIButton *signUpButton;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    
    userNameTF=[[UITextField alloc]initWithFrame:CGRectMake(100, 100, 100, 40)];
    [userNameTF setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:userNameTF];
    userNameTF.placeholder=@"User Name";
    
    passwordTextFiled=[[UITextField alloc]initWithFrame:CGRectMake(100,200, 100, 40)];
    [passwordTextFiled setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:passwordTextFiled];
    passwordTextFiled.placeholder=@"Password";
    
    
    signUpButton=[[UIButton alloc]initWithFrame:CGRectMake(100, 300, 100, 40)];
    [self.view addSubview:signUpButton];
    [signUpButton setBackgroundColor:[UIColor greenColor]];
    
    [signUpButton addTarget:self action:@selector(perfotmSignUpAction) forControlEvents:UIControlEventTouchUpInside];
    
    


}

-(void)perfotmSignUpAction{
    
    [[NSUserDefaults standardUserDefaults]setValue:userNameTF.text forKey:USERNAME];
    [[NSUserDefaults standardUserDefaults]setValue:passwordTextFiled.text forKey:PASSWORD];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ViewController *vc=[[ViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
