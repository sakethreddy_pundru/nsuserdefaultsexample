//
//  main.m
//  NSUserDefaultsExample
//
//  Created by Kvana Mac Pro 2 on 12/15/15.
//  Copyright © 2015 Kvana Mac Pro 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
